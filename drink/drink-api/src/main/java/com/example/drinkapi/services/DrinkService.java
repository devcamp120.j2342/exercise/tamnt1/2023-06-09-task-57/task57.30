package com.example.drinkapi.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.drinkapi.models.Drink;

@Service
public class DrinkService {
    public List<Drink> createDrink() {
        List<Drink> drinkList = new ArrayList<>();
        drinkList.add(new Drink("TRATAC", "Trà tắc", 10000, null, 1615177934000L, 1615177934000L));
        drinkList.add(new Drink("COCA", "Cocacola", 15000, null, 1615177934000L, 1615177934000L));
        drinkList.add(new Drink("PEPSI", "Pepsi", 15000, null, 1615177934000L, 1615177934000L));
        drinkList.add(new Drink("LAVIE", "Lavie", 5000, null, 1615177934000L, 1615177934000L));
        drinkList.add(new Drink("TRASUA", "Trà sữa trân châu", 40000, null, 1615177934000L, 1615177934000L));
        drinkList.add(new Drink("FANTA", "Fanta", 15000, null, 1615177934000L, 1615177934000L));
        return drinkList;
    }
}
