package com.example.drinkapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.drinkapi.models.Drink;
import com.example.drinkapi.services.DrinkService;

@RestController
@CrossOrigin

public class DrinkController {

    @Autowired
    DrinkService drinkService;

    @GetMapping("/drink-list")
    public List<Drink> getDrinkList() {
        return drinkService.createDrink();
    }
}
